package com.example.waytechproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ToggleButton;

public class SetActivity extends AppCompatActivity {
   ImageView iv1,iv2;
    boolean a=false,b=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set);

        iv1=(ImageView)findViewById(R.id.boy);
        iv2=(ImageView)findViewById(R.id.girl);

        iv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(b==true){
                    a=true;
                    b=false;
                    iv2.setImageResource(R.drawable.girl);
                    iv1.setImageResource(R.drawable.boy_checked);
                }else if(a==true&&b==false){
                    a=false;
                    iv1.setImageResource(R.drawable.boy);
                }else{
                    a=true;
                    iv1.setImageResource(R.drawable.boy_checked);
                }
            }
        });

        iv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(a==true){
                    a=false;
                    b=true;
                    iv1.setImageResource(R.drawable.boy);
                    iv2.setImageResource(R.drawable.girl_checked);
                }else if(b==true&&a==false){
                    b=false;
                    iv2.setImageResource(R.drawable.girl);
                }else{
                    b=true;
                    iv2.setImageResource(R.drawable.girl_checked);
                }
            }
        });
    }
}
